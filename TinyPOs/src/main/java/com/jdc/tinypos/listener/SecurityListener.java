package com.jdc.tinypos.listener;

import java.time.LocalDateTime;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.jdc.tinypos.entity.SecurityInfo;

public class SecurityListener {
	
	private String modifiedUser = "ModifiedUser";
	private String createUser="CreateUser";

	@PrePersist
	public void prePersist(Object object) {
		if(object instanceof SecureEntity) {
			SecureEntity entity = (SecureEntity) object;
			SecurityInfo sinfo = new SecurityInfo();
			sinfo.setCreation(LocalDateTime.now());
			sinfo.setDelFlg(false);
			sinfo.setCreateUser(createUser);
			entity.setSecurity(sinfo);
		}
	}
	
	@PreUpdate
	public void preUpdate(Object object) {
		if(object instanceof SecureEntity) {
			SecureEntity entity = (SecureEntity) object;
			entity.getSecurity().setModification(LocalDateTime.now());
			entity.getSecurity().setModifiedUser(modifiedUser);
		}
	}
	
}
