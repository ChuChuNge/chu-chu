package com.jdc.tinypos.repo;

import javax.persistence.EntityManager;

import com.jdc.tinypos.entity.Category;

public class CategoryRepo {
	
	private EntityManager em;
	
	public CategoryRepo(EntityManager em) {
		super();
		this.em = em;
	}
	
	public void create(Category c) {
		em.getTransaction().begin();
		em.persist(c);
		em.getTransaction().commit();
	}

}
