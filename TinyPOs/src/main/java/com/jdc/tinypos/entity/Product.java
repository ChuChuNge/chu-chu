package com.jdc.tinypos.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.OneToMany;

@Entity
public class Product implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public Product() {
    }
    @Id
    @GeneratedValue(strategy = IDENTITY)
	private int id;
    private String name;
    private String image;
    private SecurityInfo security;
    @OneToMany
	private Set<Category> category;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public SecurityInfo getSecurity() {
		return security;
	}
	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}
	public Set<Category> getCategory() {
		return category;
	}
	public void setCategory(Set<Category> category) {
		this.category = category;
	}

}