package com.jdc.tinypos.entity;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import com.jdc.tinypos.listener.SecureEntity;

import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import static javax.persistence.EnumType.STRING;

@Entity
public class Employee implements SecureEntity{

	public Employee() {
    }
    @Id
    @GeneratedValue(strategy = IDENTITY)
	private String loginId;
    private String name;
    private SecurityInfo security;
    @Enumerated(STRING)
    private Role role;
    private ContactInfo contact;
    public enum Role {
        Manager,
        Sale
    }
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public SecurityInfo getSecurity() {
		return security;
	}
	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public ContactInfo getContact() {
		return contact;
	}
	public void setContact(ContactInfo contact) {
		this.contact = contact;
	}

}