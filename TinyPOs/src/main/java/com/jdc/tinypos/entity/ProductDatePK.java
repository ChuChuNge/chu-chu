package com.jdc.tinypos.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Embeddable;

@Embeddable
public class ProductDatePK implements Serializable{

	private static final long serialVersionUID = 1L;


	public ProductDatePK() {
    }

    private LocalDate refDate;
    private int productId;
    
    
	public LocalDate getRefDate() {
		return refDate;
	}
	public void setRefDate(LocalDate refDate) {
		this.refDate = refDate;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + productId;
		result = prime * result + ((refDate == null) ? 0 : refDate.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductDatePK other = (ProductDatePK) obj;
		if (productId != other.productId)
			return false;
		if (refDate == null) {
			if (other.refDate != null)
				return false;
		} else if (!refDate.equals(other.refDate))
			return false;
		return true;
	}
	
}