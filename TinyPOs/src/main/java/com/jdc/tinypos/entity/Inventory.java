package com.jdc.tinypos.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import java.io.Serializable;
import java.util.Set;

import javax.persistence.OneToMany;

@Entity
public class Inventory implements Serializable{

	private static final long serialVersionUID = 1L;

	public Inventory() {
    }
	
	@Id
    @GeneratedValue(strategy = IDENTITY)
	private long id;
    private int stock;
    private SecurityInfo security;
    @OneToMany
	private Set<Product> product;
    private StockAction action;
    private Inventory lastStock;
    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public SecurityInfo getSecurity() {
		return security;
	}
	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	public Set<Product> getProduct() {
		return product;
	}
	public void setProduct(Set<Product> product) {
		this.product = product;
	}
	public StockAction getAction() {
		return action;
	}
	public void setAction(StockAction action) {
		this.action = action;
	}
	public Inventory getLastStock() {
		return lastStock;
	}
	public void setLastStock(Inventory lastStock) {
		this.lastStock = lastStock;
	}

}