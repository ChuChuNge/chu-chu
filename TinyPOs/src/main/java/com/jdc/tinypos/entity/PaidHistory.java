package com.jdc.tinypos.entity;

import java.util.Set;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import com.jdc.tinypos.listener.SecureEntity;
import javax.persistence.OneToMany;

@Entity
public class PaidHistory implements SecureEntity{
	
	public PaidHistory() {
    }
	
	private int paid;
    private int lastRemain;
	private SecurityInfo security;
    @OneToMany
	private Set<PurchaseHistory> history;
    
    @EmbeddedId
    private PaidHistoryPK id;
    
	public int getPaid() {
		return paid;
	}
	public void setPaid(int paid) {
		this.paid = paid;
	}
	public int getLastRemain() {
		return lastRemain;
	}
	public void setLastRemain(int lastRemain) {
		this.lastRemain = lastRemain;
	}
	public SecurityInfo getSecurity() {
		return security;
	}
	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	
	public Set<PurchaseHistory> getHistory() {
		return history;
	}
	public void setHistory(Set<PurchaseHistory> history) {
		this.history = history;
	}
	public PaidHistoryPK getId() {
		return id;
	}
	public void setId(PaidHistoryPK id) {
		this.id = id;
	}
    
}