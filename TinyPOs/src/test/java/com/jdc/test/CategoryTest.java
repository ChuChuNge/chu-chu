package com.jdc.test;

import static org.junit.jupiter.api.Assertions.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.jdc.tinypos.entity.Category;
import com.jdc.tinypos.repo.CategoryRepo;

class CategoryTest {

	static EntityManagerFactory emf;

	EntityManager em;
	CategoryRepo repo;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("TinyPOs");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@BeforeEach
	void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@AfterEach
	void finish() {
		em.close();
	}
	
	@Test
	void test() {
		Category c = new Category();
		c.setImage("categoryImage");
		c.setName("orange juice");
		em.getTransaction().begin();

		em.persist(c);

		em.getTransaction().commit();
		assertEquals(1, c.getId());
	}

}
