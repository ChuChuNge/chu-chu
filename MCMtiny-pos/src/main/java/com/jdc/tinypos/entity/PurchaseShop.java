package com.jdc.tinypos.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

@Entity
public class PurchaseShop implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public PurchaseShop() {
    }
    @Id
    @GeneratedValue(strategy = IDENTITY)
	private int id;
    private String contactPerson;
    private SecurityInfo security;
    private ContactInfo contact;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public SecurityInfo getSecurity() {
		return security;
	}
	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}
	public ContactInfo getContact() {
		return contact;
	}
	public void setContact(ContactInfo contact) {
		this.contact = contact;
	}
    
}