package com.jdc.tinypos.entity;
import javax.persistence.Entity;
import java.io.Serializable;
import javax.persistence.EmbeddedId;

@Entity
public class SellPrice implements Serializable{

	private static final long serialVersionUID = 1L;

	public SellPrice() {
    }
    
	private int price;
    private SecurityInfo security;
    private Product product;
    private SellPrice lastPrice;
    @EmbeddedId
    private ProductDatePK id;
    
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public SecurityInfo getSecurity() {
		return security;
	}
	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public SellPrice getLastPrice() {
		return lastPrice;
	}
	public void setLastPrice(SellPrice lastPrice) {
		this.lastPrice = lastPrice;
	}
	public ProductDatePK getId() {
		return id;
	}
	public void setId(ProductDatePK id) {
		this.id = id;
	}
    
}