package com.jdc.tinypos.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Embeddable;

@Embeddable
public class PaidHistoryPK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private long historyId;
    private LocalDate refDate;
    
	public PaidHistoryPK() {
		super();
	}
	
	public long getHistoryId() {
		return historyId;
	}
	public void setHistoryId(long historyId) {
		this.historyId = historyId;
	}
	public LocalDate getRefDate() {
		return refDate;
	}
	public void setRefDate(LocalDate refDate) {
		this.refDate = refDate;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (historyId ^ (historyId >>> 32));
		result = prime * result + ((refDate == null) ? 0 : refDate.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaidHistoryPK other = (PaidHistoryPK) obj;
		if (historyId != other.historyId)
			return false;
		if (refDate == null) {
			if (other.refDate != null)
				return false;
		} else if (!refDate.equals(other.refDate))
			return false;
		return true;
	}
    
}