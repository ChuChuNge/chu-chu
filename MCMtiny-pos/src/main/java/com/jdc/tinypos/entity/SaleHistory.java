package com.jdc.tinypos.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class SaleHistory extends StockAction {
   
	private static final long serialVersionUID = 1L;
	public SaleHistory() {
    }
	
	private long id;
    private int subTotal;
    private int tax;
    private int total;
    private SecurityInfo security;
    @ManyToOne
	private Employee salePerson;
    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(int subTotal) {
		this.subTotal = subTotal;
	}
	public int getTax() {
		return tax;
	}
	public void setTax(int tax) {
		this.tax = tax;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public SecurityInfo getSecurity() {
		return security;
	}
	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}
	public Employee getSalePerson() {
		return salePerson;
	}
	public void setSalePerson(Employee salePerson) {
		this.salePerson = salePerson;
	}

}