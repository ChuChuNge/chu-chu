package com.jdc.tinypos.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.OneToMany;
import javax.persistence.Enumerated;
import static javax.persistence.EnumType.STRING;
@Entity
public class StockAction implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public StockAction() {
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
	private long id;
    private LocalDate refDate;
    private int price;
    private int quentity;
    private SecurityInfo security;
    @OneToMany
	private Set<Product> product;
    @Enumerated(STRING)
	private Action action;
    
    public enum Action {
        Sale,
        Purchase
    }

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDate getRefDate() {
		return refDate;
	}

	public void setRefDate(LocalDate refDate) {
		this.refDate = refDate;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getQuentity() {
		return quentity;
	}

	public void setQuentity(int quentity) {
		this.quentity = quentity;
	}

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	public Set<Product> getProduct() {
		return product;
	}

	public void setProduct(Set<Product> product) {
		this.product = product;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}
    
}