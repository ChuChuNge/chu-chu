package com.jdc.tinypos.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class PurchaseHistory extends StockAction {

	private static final long serialVersionUID = 1L;

	public PurchaseHistory() {
    }

    private long id;
    private int subTotal;
    private int tax;
    private int total;
    private int paid;
    private SecurityInfo security;
	@ManyToOne
	private PurchaseShop shop;
    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(int subTotal) {
		this.subTotal = subTotal;
	}
	public int getTax() {
		return tax;
	}
	public void setTax(int tax) {
		this.tax = tax;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getPaid() {
		return paid;
	}
	public void setPaid(int paid) {
		this.paid = paid;
	}
	public SecurityInfo getSecurity() {
		return security;
	}
	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}
	public PurchaseShop getShop() {
		return shop;
	}
	public void setShop(PurchaseShop shop) {
		this.shop = shop;
	}
    
}