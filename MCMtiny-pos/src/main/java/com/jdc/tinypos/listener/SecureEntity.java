package com.jdc.tinypos.listener;

import com.jdc.tinypos.entity.SecurityInfo;

public interface SecureEntity {

	SecurityInfo getSecurity();

	void setSecurity(SecurityInfo security);
}
