package com.jdc.tinypos.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.jdc.tinypos.SpringJpaApplication;
import com.jdc.tinypos.entity.Person;
import com.jdc.tinypos.repo.PersonRepo;

class SpringJpaApplicationTest {
	
	private static ConfigurableApplicationContext ctx;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ctx = new AnnotationConfigApplicationContext(SpringJpaApplication.class);
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		ctx.close();
	}
	

	@Test
	void test() {
		PersonRepo repo = ctx.getBean(PersonRepo.class);
		Person p = new Person();
		p.setName("Jhon Snow");
		repo.create(p);
		
		assertEquals(1, p.getId());
	}

}
