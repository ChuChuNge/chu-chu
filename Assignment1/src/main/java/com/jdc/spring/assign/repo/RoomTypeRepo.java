package com.jdc.spring.assign.repo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.jdc.spring.assign.entity.RoomType;

@Repository
public class RoomTypeRepo {

	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public void create(RoomType rt) {
		em.persist(rt);
	}
	
	@Transactional
	public void update(RoomType rt) {
		em.merge(rt);
	}
	
	@Transactional
	public void delete(int id) {
		em.remove(em.find(RoomType.class, id));
	}
	
	public RoomType findById(int id) {
		return em.find(RoomType.class, id);
		
	}
}
