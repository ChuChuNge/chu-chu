package com.jdc.spring.assign.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Embeddable;

@Embeddable
public class RoomReservationPK implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long reservationId;
	private int roomId;
	private LocalDate stayDate;
	
	public RoomReservationPK() {
		super();
	}

	public Long getReservationId() {
		return reservationId;
	}

	public void setReservationId(Long reservationId) {
		this.reservationId = reservationId;
	}

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	public LocalDate getStayDate() {
		return stayDate;
	}

	public void setStayDate(LocalDate stayDate) {
		this.stayDate = stayDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((reservationId == null) ? 0 : reservationId.hashCode());
		result = prime * result + roomId;
		result = prime * result + ((stayDate == null) ? 0 : stayDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoomReservationPK other = (RoomReservationPK) obj;
		if (reservationId == null) {
			if (other.reservationId != null)
				return false;
		} else if (!reservationId.equals(other.reservationId))
			return false;
		if (roomId != other.roomId)
			return false;
		if (stayDate == null) {
			if (other.stayDate != null)
				return false;
		} else if (!stayDate.equals(other.stayDate))
			return false;
		return true;
	}
	
	
}
