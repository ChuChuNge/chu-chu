package com.jdc.spring.assign.repo;

public interface BaseRepository<T> {

	void create(T t);
	void update(T t);
	void delete(T t);
	Object findById(Object id);
}
