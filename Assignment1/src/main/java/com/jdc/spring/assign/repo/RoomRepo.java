package com.jdc.spring.assign.repo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.jdc.spring.assign.entity.Room;

@Repository
public class RoomRepo {

	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public void create(Room r) {
		em.persist(r);
	}
	
	@Transactional
	public void update(Room r) {
		em.merge(r);
	}
	
	@Transactional
	public void delete(int id) {
		em.remove(em.find(Room.class, id));
	}
	
	@Transactional
	public Room findById(int id) {
		return em.find(Room.class, id);
		
	}
	
	@Transactional
	public List<Room> findByName(String name){
		return em.createNamedQuery("roomFindByName", Room.class)
				.setParameter("name", name)
				.getResultList();
				 
	}
}
