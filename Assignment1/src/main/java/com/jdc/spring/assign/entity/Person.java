package com.jdc.spring.assign.entity;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class Person implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int id;
	private String name;
	private static final long serialVersionUID = 1L;

	public Person() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
   
}
