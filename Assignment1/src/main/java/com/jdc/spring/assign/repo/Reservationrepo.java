package com.jdc.spring.assign.repo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.jdc.spring.assign.entity.Reservation;

@Repository
public class Reservationrepo {

	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public void create(Reservation r) {
		em.persist(r);
	}
	
	@Transactional
	public void update(Reservation r) {
		em.merge(r);
	}
	
	@Transactional
	public void delete(long id) {
		em.remove(em.find(Reservation.class, id));
	}
	
	public Reservation findById(long id) {
		return em.find(Reservation.class, id);
		
	}
}
