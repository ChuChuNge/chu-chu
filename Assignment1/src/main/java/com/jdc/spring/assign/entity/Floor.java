package com.jdc.spring.assign.entity;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * Entity implementation class for Entity: Floor
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="flfindByName", query="select f from Floor f where f.name = :name")
})
public class Floor implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int id;
	private String name;
	@OneToMany(mappedBy = "floor")
	private List<Room> room;
	
	public Floor() {
		super();
	}   
	
	public List<Room> getRoom() {
		return room;
	}
	public void setRoom(List<Room> room) {
		this.room = room;
	}
	
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
   
}
