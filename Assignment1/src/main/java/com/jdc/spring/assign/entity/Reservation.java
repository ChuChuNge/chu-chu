package com.jdc.spring.assign.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.*;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * Entity implementation class for Entity: Reservation
 *
 */
@Entity

public class Reservation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long id;
	private LocalDate reserveDate;
	private LocalDate checkInDate;
	private LocalDate checkOutDate;
	private int subTotal;
	private int tax;
	private int serviceCharge;
	private int discount;
	private int total;
	
	@OneToMany(mappedBy = "reservation")
	private List<RoomReservation> rooms;
	
	public Reservation() {
		super();
	}   
	public long getId() {
		return this.id;
	}

	public List<RoomReservation> getRooms() {
		return rooms;
	}
	public void setRooms(List<RoomReservation> rooms) {
		this.rooms = rooms;
	}
	public void setId(long id) {
		this.id = id;
	}   
	public LocalDate getReserveDate() {
		return this.reserveDate;
	}

	public void setReserveDate(LocalDate reserveDate) {
		this.reserveDate = reserveDate;
	}   
	public LocalDate getCheckInDate() {
		return this.checkInDate;
	}

	public void setCheckInDate(LocalDate checkInDate) {
		this.checkInDate = checkInDate;
	}   
	public LocalDate getCheckOutDate() {
		return this.checkOutDate;
	}

	public void setCheckOutDate(LocalDate checkOutDate) {
		this.checkOutDate = checkOutDate;
	}   
	public int getSubTotal() {
		return this.subTotal;
	}

	public void setSubTotal(int subTotal) {
		this.subTotal = subTotal;
	}   
	public int getTax() {
		return this.tax;
	}

	public void setTax(int tax) {
		this.tax = tax;
	}   
	public int getServiceCharge() {
		return this.serviceCharge;
	}

	public void setServiceCharge(int serviceCharge) {
		this.serviceCharge = serviceCharge;
	}   
	public int getDiscount() {
		return this.discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}   
	public int getTotal() {
		return this.total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
   
}
