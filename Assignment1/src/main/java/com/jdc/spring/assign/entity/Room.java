package com.jdc.spring.assign.entity;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * Entity implementation class for Entity: Room
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="roomFindByName", query="select r from Room r where r.name = :name")
})
public class Room implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int id;
	private String name;
	private String roomNumber;
	
	@ManyToOne
	private Floor floor;
	
	@ManyToOne
	private RoomType type;
	
	@OneToMany(mappedBy = "rooms")
	private List<RoomReservation> reservation;
	
	public Room() {
		super();
	}   
	
	public List<RoomReservation> getReservation() {
		return reservation;
	}


	public void setReservation(List<RoomReservation> reservation) {
		this.reservation = reservation;
	}


	public Floor getFloor() {
		return floor;
	}


	public void setFloor(Floor floor) {
		this.floor = floor;
	}


	public RoomType getType() {
		return type;
	}


	public void setType(RoomType type) {
		this.type = type;
	}


	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public String getRoomNumber() {
		return this.roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
   
}
