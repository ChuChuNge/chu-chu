package com.jdc.spring.assign.repo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.jdc.spring.assign.entity.Floor;

@Repository
public class FloorRepo {

	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public void create(Floor f) {
		em.persist(f);
	}
	
	@Transactional
	public void update(Floor f) {
		em.merge(f);
	}
	
	@Transactional
	public void delete(int id) {
		em.remove(em.find(Floor.class, id));
	}
	
	public Floor findById(int id) {
		return em.find(Floor.class, id);
		
	}

	public List<Floor> findByName(String name1){
		List<Floor> l = em.createNamedQuery("flfindByName", Floor.class)
				.setParameter("name", name1)
				.getResultList();
		 return l;
	}
}
