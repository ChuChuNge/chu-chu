package com.jdc.spring.assign.repo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.jdc.spring.assign.entity.Person;

@Repository
public class PersonRepo {

	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public void create(Person p) {
		em.persist(p);
	}
}
