package com.jdc.spring.assign.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: RoomReservation
 *
 */
@Entity

public class RoomReservation implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private RoomReservationPK id;
	
	@ManyToOne
	private Room rooms;
	
	@ManyToOne
	private Reservation reservation;
	
	public RoomReservation() {
		super();
	}

	public RoomReservationPK getId() {
		return id;
	}

	public void setId(RoomReservationPK id) {
		this.id = id;
	}

	public Room getRooms() {
		return rooms;
	}

	public void setRooms(Room rooms) {
		this.rooms = rooms;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}
	
	
   
}
