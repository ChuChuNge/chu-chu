package com.jdc.spring.assign.repo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Repository;
import com.jdc.spring.assign.entity.Customer;

@Repository
public class CustomerRepo {

	@PersistenceContext
	private EntityManager em;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Transactional
	public void create(Customer c) {
		em.persist(c);
	}
	
	@Transactional
	public void update(Customer c) {
		em.merge(c);
	}
	
	@Transactional
	public void delete(long id) {
		em.remove(em.find(Customer.class, id));
	}
	
	public Customer findById(long id) {
		return em.find(Customer.class, id);
		
	}
	
	public List<Customer> findByName(String name){
		return em.createNamedQuery("findByName", Customer.class)
				.setParameter("name", name)
				.getResultList();
				 
	}
	
	/*@Transactional
	public void create(Customer t) {
		// TODO Auto-generated method stub
		
		em.persist(t);
	}
	
	@Transactional
	@Override
	public void update(Object t) {
		// TODO Auto-generated method stub
		em.merge(t);
	}
	
	@Transactional
	@Override
	public void delete(long id) {
		// TODO Auto-generated method stub
		em.remove(em.find(Customer.class, id));
	}
	
	@Transactional
	@Override
	public Object findById(long id) {
		// TODO Auto-generated method stub
		return em.find(Customer.class, id);
	}

	@Override
	public void create(Object t) {
		// TODO Auto-generated method stub
		
	}*/
}
