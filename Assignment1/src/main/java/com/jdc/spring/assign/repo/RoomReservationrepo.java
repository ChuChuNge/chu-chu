package com.jdc.spring.assign.repo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.jdc.spring.assign.entity.RoomReservation;

@Repository
public class RoomReservationrepo {

	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public void create(RoomReservation rv) {
		em.persist(rv);
	}
	
	@Transactional
	public void update(RoomReservation rv) {
		em.merge(rv);
	}
	
	@Transactional
	public void delete(int id) {
		em.remove(em.find(RoomReservation.class, id));
	}
	
	public RoomReservation findById(int id) {
		return em.find(RoomReservation.class, id);
		
	}
}
