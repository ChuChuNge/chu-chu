package com.jdc.spring.assign.entity;

import java.io.Serializable;
import java.lang.String;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.*;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * Entity implementation class for Entity: RoomType
 *
 */
@Entity

public class RoomType implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int id;
	private String type;
	private int price;
	
	@ElementCollection
	@CollectionTable
	@MapKeyColumn
	private Map<String, String> facilities;
	
	@OneToMany(mappedBy = "type")
	private List<Room> room;
	
	public RoomType() {
		super();
		facilities = new LinkedHashMap<>();
	}   
	
	public List<Room> getRoom() {
		return room;
	}


	public void setRoom(List<Room> room) {
		this.room = room;
	}


	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}   
	
	public Map<String, String> getFacilities() {
		return facilities;
	}
	public void setFacilities(Map<String, String> facilities) {
		this.facilities = facilities;
	}
	public int getPrice() {
		return this.price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
   
}
