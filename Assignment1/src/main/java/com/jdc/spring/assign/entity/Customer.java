package com.jdc.spring.assign.entity;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;
import static javax.persistence.EnumType.STRING;

/**
 * Entity implementation class for Entity: Customer
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="findByName", query="select c from Customer c where c.name = :name")
})

public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long id;
	private String name;
	private String phone;
	private String email;
	private String passportOrNrc;
	private String nationality;
	
	@Enumerated(STRING)
	private Gender gender;
	
	public Customer() {
		super();
	}   
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}   
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}   
	public String getPassportOrNrc() {
		return this.passportOrNrc;
	}

	public void setPassportOrNrc(String passportOrNrc) {
		this.passportOrNrc = passportOrNrc;
	}   
	public String getNationality() {
		return this.nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
}
