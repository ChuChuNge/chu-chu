package com.jdc.spring.assign.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.jdc.spring.assign.entity.Customer;
import com.jdc.spring.assign.entity.Gender;
import com.jdc.spring.assign.entity.SpringJpaApplication;
import com.jdc.spring.assign.repo.CustomerRepo;

class CustomerRepoTest {

	private static ConfigurableApplicationContext ctx;
	
	private Customer customer = new Customer();
	CustomerRepo repo = ctx.getBean(CustomerRepo.class);
	
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ctx = new AnnotationConfigApplicationContext(SpringJpaApplication.class);
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		ctx.close();
	}

	@Test
	@DisplayName("create")
	void test() {
		customer.setEmail("chu@gmail.com");
		customer.setGender(Gender.Male);
		customer.setName("AungKung");
		customer.setNationality("Myanmar");
		customer.setPassportOrNrc("9/PTT87635");
		customer.setPhone("09456788");
		
		repo.create(customer);
		assertEquals(1, customer.getId());
	}
	
	@Test
	@DisplayName("findById")
	void test1() {
		customer = repo.findById(1);
		assertEquals("AungKung", customer.getName());
		assertEquals("Myanmar", customer.getNationality());
	}
	
	@Test
	@DisplayName("update")
	void test2() {
		customer = repo.findById(1);
		customer.setName("AungMyint");
		repo.update(customer);
		assertEquals("AungMyint", customer.getName());
	}

	@Test
	@DisplayName("delete")
	void test3() {
		repo.delete(1);
		customer = repo.findById(1);
		assertNull(customer);
	}
	
}
