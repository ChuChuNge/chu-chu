package com.jdc.spring.assign.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.jdc.spring.assign.entity.Floor;
import com.jdc.spring.assign.entity.SpringJpaApplication;
import com.jdc.spring.assign.repo.FloorRepo;

class FloorRepoTest {

	private static ConfigurableApplicationContext ctx;

	FloorRepo repo = ctx.getBean(FloorRepo.class);

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ctx = new AnnotationConfigApplicationContext(SpringJpaApplication.class);
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		ctx.close();
	}

	@Test
	@DisplayName("create")
	public void test1() {

		Floor f = new Floor();
		f.setName("first Floor");
		repo.create(f);
		assertEquals(1, f.getId());
	}

	@Test
	@DisplayName("findById")
	public void test2() {
		Floor f = repo.findById(1);
		assertEquals("first Floor", f.getName());

	}

	@Test
	@DisplayName("update")
	public void test3() {
		Floor f = repo.findById(1);
		f.setName("Second Floor");
		repo.update(f);
		assertEquals("Second Floor", f.getName());

	}

	@Test
	@DisplayName("delete")
	public void test4() {
		repo.delete(1);
		Floor f = repo.findById(1);
		assertNull(f);
	}

}
