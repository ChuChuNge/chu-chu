package com.jdc.spring.assign.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.jdc.spring.assign.entity.Floor;
import com.jdc.spring.assign.entity.Person;
import com.jdc.spring.assign.entity.Room;
import com.jdc.spring.assign.entity.RoomType;
import com.jdc.spring.assign.entity.SpringJpaApplication;
import com.jdc.spring.assign.repo.FloorRepo;
import com.jdc.spring.assign.repo.PersonRepo;
import com.jdc.spring.assign.repo.RoomRepo;
import com.jdc.spring.assign.repo.RoomTypeRepo;

class SpringJpaApplicationTest {
	
	private static ConfigurableApplicationContext ctx;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ctx = new AnnotationConfigApplicationContext(SpringJpaApplication.class);
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		ctx.close();
	}
	

	@Test
	void test() {
		
		FloorRepo frepo = ctx.getBean(FloorRepo.class);
		Floor f = new Floor();
		f.setName("First Floor");
		frepo.create(f);
		
		RoomTypeRepo rmrepo = ctx.getBean(RoomTypeRepo.class);
		RoomType rt = new RoomType();
		rt.setPrice(50000);
		rt.setType("Family");
		rmrepo.create(rt);
		
		Room r = new Room();
		r.setName("East");
		r.setRoomNumber("123");
		r.setFloor(f);
		r.setType(rt);
		
		RoomRepo repo = ctx.getBean(RoomRepo.class);
		repo.create(r);
		assertEquals(1, r.getId());
	}

	@Test
	void testPerson() {
		PersonRepo repo = ctx.getBean(PersonRepo.class);
		Person p = new Person();
		p.setName("Jhon Snow");
		repo.create(p);
		
		assertEquals(1, p.getId());
	}
}
