 package com.jdc.spring.assign.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.jdc.spring.assign.entity.RoomType;
import com.jdc.spring.assign.entity.SpringJpaApplication;
import com.jdc.spring.assign.repo.RoomTypeRepo;

class RoomTypeRepoTest {

	private static ConfigurableApplicationContext ctx;
	private RoomType type = new RoomType();
	RoomTypeRepo repo = ctx.getBean(RoomTypeRepo.class);

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ctx = new AnnotationConfigApplicationContext(SpringJpaApplication.class);
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		ctx.close();
	}

	@Test
	@DisplayName("create")
	public void test1() {

		type.setPrice(300);
		type.setType("Single");
		repo.create(type);
		assertEquals(1, type.getId());

	}

	@Test
	@DisplayName("findById")
	public void test2() {

		type = repo.findById(1);
		assertNotNull(type); 
		assertEquals(300, type.getPrice());
		assertEquals("Single", type.getType());
	}

	@Test
	@DisplayName("update")
	public void test3() {

		type = repo.findById(1);
		type.setPrice(500);
		type.setType("Double");
		repo.update(type);
		assertEquals(500, type.getPrice());
		assertEquals("Double", type.getType());

	}
	@Test
	@DisplayName("delete")
	void test4() {
		repo.delete(1);
		type = repo.findById(1);
		assertNull(type);
	}

}
