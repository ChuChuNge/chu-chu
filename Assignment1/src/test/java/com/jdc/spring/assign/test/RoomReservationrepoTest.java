package com.jdc.spring.assign.test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.jdc.spring.assign.entity.Floor;
import com.jdc.spring.assign.entity.Reservation;
import com.jdc.spring.assign.entity.Room;
import com.jdc.spring.assign.entity.RoomReservation;
import com.jdc.spring.assign.entity.RoomType;
import com.jdc.spring.assign.entity.SpringJpaApplication;
import com.jdc.spring.assign.repo.FloorRepo;
import com.jdc.spring.assign.repo.Reservationrepo;
import com.jdc.spring.assign.repo.RoomRepo;
import com.jdc.spring.assign.repo.RoomReservationrepo;
import com.jdc.spring.assign.repo.RoomTypeRepo;

class RoomReservationrepoTest {

	private static ConfigurableApplicationContext ctx;

	RoomRepo repo = ctx.getBean(RoomRepo.class);
	RoomTypeRepo typeRepo = ctx.getBean(RoomTypeRepo.class);
	FloorRepo floorRepo = ctx.getBean(FloorRepo.class);
	RoomReservationrepo reser_repo = ctx.getBean(RoomReservationrepo.class);
	Reservationrepo resrepo = ctx.getBean(Reservationrepo.class);
	private RoomType type = new RoomType();
	private Floor floor = new Floor();
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ctx = new AnnotationConfigApplicationContext(SpringJpaApplication.class);
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		ctx.close();
	}

	@Test
	@DisplayName("create")
	void test() {
		

		type.setPrice(750);
		type.setType("double");
		typeRepo.create(type);

		floor.setName("firstFloor");
		floorRepo.create(floor);

		Room room = new Room();
		room.setName("East");
		room.setRoomNumber("111");
		room.setType(type);
		room.setFloor(floor);
		repo.create(room);
		
		Reservation r = new Reservation();
		r.setCheckInDate(LocalDate.now());
		r.setCheckOutDate(LocalDate.now());
		r.setDiscount(500);
		r.setReserveDate(LocalDate.now());
		r.setServiceCharge(500);
		r.setSubTotal(r.getServiceCharge()-r.getDiscount());
		r.setTax(200);
		r.setTotal(r.getSubTotal()+r.getTax());
		
		resrepo.create(r);
		RoomReservation roomres = new RoomReservation();
		roomres.setReservation(r);
		roomres.setRooms(room);
		
		assertEquals(1, roomres.getId());
	}
	
	@Test
	@DisplayName("findById")
	void test1() {
		//RoomReservation r = reser_repo.findById(1);
		//assertEquals("", actual);
	}
	
	@Test
	@DisplayName("update")
	void test2() {
		fail("Not yet implemented");
	}

	@Test
	@DisplayName("delete")
	void test3() {
		reser_repo.delete(1);
		RoomReservation r = reser_repo.findById(1);
		assertNull(r);
	}
}
