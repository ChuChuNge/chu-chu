package com.jdc.spring.assign.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.jdc.spring.assign.entity.Floor;
import com.jdc.spring.assign.entity.Room;
import com.jdc.spring.assign.entity.RoomType;
import com.jdc.spring.assign.entity.SpringJpaApplication;
import com.jdc.spring.assign.repo.FloorRepo;
import com.jdc.spring.assign.repo.RoomRepo;
import com.jdc.spring.assign.repo.RoomTypeRepo;

class RoomRepoTest {
	private static ConfigurableApplicationContext ctx;
	RoomRepo repo = ctx.getBean(RoomRepo.class);
	RoomTypeRepo typeRepo = ctx.getBean(RoomTypeRepo.class);
	FloorRepo floorRepo = ctx.getBean(FloorRepo.class);
	private RoomType type = new RoomType();
	private Floor floor = new Floor();

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ctx = new AnnotationConfigApplicationContext(SpringJpaApplication.class);
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		ctx.close();
	}

	@Test
	@DisplayName("create")
	public void test1() {

		type.setPrice(750);
		type.setType("double");
		typeRepo.create(type);

		floor.setName("firstFloor");
		floorRepo.create(floor);

		Room room = new Room();
		room.setName("East");
		room.setRoomNumber("111");
		room.setType(type);
		room.setFloor(floor);
		repo.create(room);
		assertEquals(1, room.getId());
	}


	@Test
	@DisplayName("findById")
	public void test2() {
		Room r = repo.findById(1);
		assertEquals("East", r.getName());
		assertEquals("111", r.getRoomNumber());

	}


	@Test
	@DisplayName("update")
	public void test3() {
		Room r = repo.findById(1);
		r.setName("Hello");
		repo.update(r);
		assertEquals("Hello", r.getName());

	}

	@Test
	@DisplayName("delete")
	public void test4() {

		repo.delete(1);
		Room r = repo.findById(1);
		assertNull(r);
	}

}
