package com.jdc.spring.assign.test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.jdc.spring.assign.entity.Reservation;
import com.jdc.spring.assign.entity.SpringJpaApplication;
import com.jdc.spring.assign.repo.Reservationrepo;

class ReservationrepoTest {

	private static ConfigurableApplicationContext ctx;

	Reservationrepo repo = ctx.getBean(Reservationrepo.class);
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ctx = new AnnotationConfigApplicationContext(SpringJpaApplication.class);
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		ctx.close();
	}

	@Test
	@DisplayName("create")
	void test() {
		Reservation r = new Reservation();
		r.setCheckInDate(LocalDate.now());
		r.setCheckOutDate(LocalDate.now());
		r.setDiscount(500);
		r.setReserveDate(LocalDate.now());
		r.setServiceCharge(500);
		r.setSubTotal(r.getServiceCharge()-r.getDiscount());
		r.setTax(200);
		r.setTotal(r.getSubTotal()+r.getTax());
		
		repo.create(r);
		assertEquals(1, r.getId());
	}
	
	@Test
	@DisplayName("findById")
	void test1() {
		Reservation r = repo.findById(1);
		assertEquals(200, r.getTax());
		
	}
	
	@Test
	@DisplayName("update")
	void test2() {
		Reservation r = repo.findById(1);
		r.setTax(500);
		repo.update(r);
		assertEquals(500, r.getTax());
	}
	
	@Test
	@DisplayName("delete")
	void test3() {
		repo.delete(1);
		Reservation r = repo.findById(1);
		assertNull(r);
	}

}
